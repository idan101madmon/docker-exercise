# Start from a OS image with the latest version of Go installed
# and a workspace (GOPATH) configured at /go.
FROM golang:alpine

# create the WORKDIR folder
RUN mkdir /app

#copy the files to /app
ADD . /app/

#set the WORKDIR and cd there
WORKDIR /app

#build the app
RUN go build -o main

#create user so the app won't run on root
RUN adduser -S -D -H -h /app appuser
USER appuser

#run the app
CMD ["./main"]